import json
from math import exp
from PIL import Image

from parameters import get_pixels, getWeights, getBiases


def main():
    """
    Sizes of this neural network:
    Input: 900 neurons
    Layer1: 200 neurons
    Layer2: 200 neurons
    Layer3: 36 neurons (Output)
    """
    weightsLayer1 = getWeights(1)  #weightsLayer1[200][900]
    weightsLayer2 = getWeights(2)  #weightsLayer2[200][200]
    weightsLayer3 = getWeights(3)  #weightsLayer3[36][200]
    biasesLayer1 = getBiases(1)    #biasesLayer1[200]
    biasesLayer2 = getBiases(2)    #biasesLayer2[200]
    biasesLayer3 = getBiases(3)    #biasesLayer3[36]

    pixels = get_pixels('test_data/Sample016/img016-00013.png')  #pixels[900]


        
if __name__ == '__main__':
    main()
