import json
from math import exp
from PIL import Image

from parameters import get_pixels, getWeights, getBiases

def sigmoid(x):
    return 1/(1+exp(-x))

def feed(inputs, weights, biases):
    output = []
    for i in range(len(biases)):
        output.append(0)
        for j in range(len(inputs)):
            output[i] += weights[i][str(j)]*inputs[j]
        output[i] += biases[i]
        output[i] = sigmoid(output[i])
    return output
    


def main():

    weightsLayer1 = getWeights(1)
    weightsLayer2 = getWeights(2)
    weightsLayer3 = getWeights(3)
    biasesLayer1 = getBiases(1)
    biasesLayer2 = getBiases(2)
    biasesLayer3 = getBiases(3)

    pixels = get_pixels('../test_data/Sample016/img016-00013.png')

    hiddenlayer1 = feed(pixels, weightsLayer1, biasesLayer1)
    hiddenlayer2 = feed(hiddenlayer1, weightsLayer2, biasesLayer2)
    output = feed(hiddenlayer2, weightsLayer3, biasesLayer3)

    for i in range(35):
        print(str(i) + ": " + str(output[i]))
        
if __name__ == '__main__':
    main()
