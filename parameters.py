import json
from math import exp
from PIL import Image

def get_pixels(filename):
    pixels = list(Image.open(filename).getdata())
    output = []
    for i in range(len(pixels)):
        output.append(1-pixels[i]/255)
    return output
    

def getWeights(layer):
    return [ json_parameters["layers"][layer][str(i)]["weights"] for i in range(network_sizes[layer]) ]

def getBiases(layer):
    return [ json_parameters["layers"][layer][str(i)]["bias"] for i in range(network_sizes[layer]) ]



with open('parameters.json', 'r') as f:
    json_parameters = json.load(f)

network_sizes = json_parameters["sizes"];
